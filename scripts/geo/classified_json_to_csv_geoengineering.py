import json
import re
from collections import defaultdict
from matplotlib import pyplot as plt
import numpy as np
from tqdm import tqdm
import pickle

models = ['cardiff-stance-climate', 'cardiff-offensive',  # 'cards',
          'cardiff-sentiment', 'bertweet-sentiment',
          'geomotions-orig', 'geomotions-ekman', 'cardiff-emotion', 'bertweet-emotions', 'nrc']

subquery_ids = ['g_01', 'g_02', 'g_05', 'g_06', 's_01', 's_02', 's_21', 's_22',
       's_27', 's_04', 's_07', 's_09', 's_20', 's_23', 's_24', 's_10',
       's_11', 's_25', 's_30', 's_12', 's_14', 's_16', 's_17', 's_18',
       's_19', 's_29', 'c_01', 'c_02', 'c_03', 'c_04', 'c_05', 'c_06',
       'c_07', 'c_08', 'c_55', 'c_10', 'c_11', 'c_12', 'c_13', 'c_14',
       'c_15', 'c_16', 'c_17', 'c_18', 'c_19', 'c_54', 'c_09', 'c_20',
       'c_21', 'c_22', 'c_50', 'c_23', 'c_49', 'c_24', 'c_25', 'c_26',
       'c_51', 'c_27', 'c_29', 'c_30', 'c_31', 'c_32', 'c_33', 'c_36',
       'c_37', 'c_38', 'c_39', 'c_40', 'c_41', 'c_42', 'c_43', 'c_52',
       'c_53', 'c_44', 'c_45', 'c_46', 'c_47', 'c_48']

stats = {
    model: defaultdict(int)
    for model in models
}
cooc_stats = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))


FILE_TWEETS = 'tweets_classified2.jsonl'
FILE_OUT = 'tweets_classified5.csv'


with open(FILE_TWEETS, 'r') as f_in, \
        open(FILE_OUT, 'w') as f_out:
    f_out.write('lid,tid,uid,created_at,text,' + ','.join(models) + ',nrc-highest,nrc-score,')
    f_out.write(','.join(subquery_ids) + ',retweet_count,reply_count,like_count,quote_count\n')
    
    li = 0
    for line in tqdm(f_in):
        li += 1
        tweet = json.loads(line)
        f_out.write(str(li) + ',' + tweet['id'] + ',' + tweet['author_id'] + ',' + tweet['created_at'] + ',')
        f_out.write('"' + re.sub(r'(\s+|,)', ' ', tweet['text']).replace('"', "'") + '",')
        for model in models:
            labels = list(tweet['classes'][model].keys())
            f_out.write('|'.join(labels) + ',')
            for label in labels:
                stats[model][label] += 1

            for model_cooc in models:
                if model_cooc != model:
                    labels_cooc = list(tweet['classes'][model_cooc].keys())
                    for label_cooc in labels_cooc:
                        for label in labels:
                            cooc_stats[model][label][model_cooc][label_cooc] += 1

        em_dict = tweet['classes']['nrc']
        key = max(em_dict, key=em_dict.get) if em_dict else None
        if key:
            f_out.write(key + ',' + str(em_dict[key]) + ',')
        else:
            f_out.write('neutral' + ',' + str(np.nan) + ',')
        # write subquery info
        f_out.write(','.join(["1" if i in tweet['sid'] else "0" for i in subquery_ids]))
        # write retweet, like, reply, quotes count
        f_out.write(',' + str(tweet['public_metrics']['retweet_count']) + ',')
        f_out.write(str(tweet['public_metrics']['reply_count']) + ',')
        f_out.write(str(tweet['public_metrics']['like_count']) + ',')
        f_out.write(str(tweet['public_metrics']['quote_count']) + '\n')
        
        #if li > 100:
        #     break

print(li)

print(stats)


