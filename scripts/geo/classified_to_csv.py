import json
import re
from collections import defaultdict
from matplotlib import pyplot as plt
import numpy as np
from tqdm import tqdm
import pickle

models = ['cardiff-stance-climate', 'cardiff-offensive',  # 'cards',
          'cardiff-sentiment', 'bertweet-sentiment',
          'geomotions-orig', 'geomotions-ekman', 'cardiff-emotion', 'bertweet-emotions', 'nrc']

subquery_ids = ['g_01', 'g_02', 'g_05', 'g_06', 's_01', 's_02', 's_21', 's_22',
       's_27', 's_04', 's_07', 's_09', 's_20', 's_23', 's_24', 's_10',
       's_11', 's_25', 's_30', 's_12', 's_14', 's_16', 's_17', 's_18',
       's_19', 's_29', 'c_01', 'c_02', 'c_03', 'c_04', 'c_05', 'c_06',
       'c_07', 'c_08', 'c_55', 'c_10', 'c_11', 'c_12', 'c_13', 'c_14',
       'c_15', 'c_16', 'c_17', 'c_18', 'c_19', 'c_54', 'c_09', 'c_20',
       'c_21', 'c_22', 'c_50', 'c_23', 'c_49', 'c_24', 'c_25', 'c_26',
       'c_51', 'c_27', 'c_29', 'c_30', 'c_31', 'c_32', 'c_33', 'c_36',
       'c_37', 'c_38', 'c_39', 'c_40', 'c_41', 'c_42', 'c_43', 'c_52',
       'c_53', 'c_44', 'c_45', 'c_46', 'c_47', 'c_48']

stats = {
    model: defaultdict(int)
    for model in models
}
cooc_stats = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))


FILE_TWEETS = 'tweets_classified2.jsonl'
FILE_OUT = 'tweets_classified.csv'


with open(FILE_TWEETS, 'r') as f_in, \
        open(FILE_OUT, 'w') as f_out:
    f_out.write('id,' + ','.join(models) + ',text,subqueries\n')
    li = 0
    for line in tqdm(f_in):
        li += 1
        tweet = json.loads(line)
        f_out.write(str(li) + ',')
        for model in models:
            labels = list(tweet['classes'][model].keys())
            f_out.write('|'.join(labels) + ',')
            for label in labels:
                stats[model][label] += 1

            for model_cooc in models:
                if model_cooc != model:
                    labels_cooc = list(tweet['classes'][model_cooc].keys())
                    for label_cooc in labels_cooc:
                        for label in labels:
                            cooc_stats[model][label][model_cooc][label_cooc] += 1
        f_out.write(re.sub(r'(\s+|,)', ' ', tweet['text']) + ',' + str(tweet['sid']) + '\n')
        # if li > 1000:
        #     break

print(li)

print(stats)

#with open("correlation_between_models.pkl", 'wb') as handle:
#    pickle.dump(cooc_stats, handle)

# print(json.dumps(stats, indent=3))
# print(json.dumps(cooc_stats, indent=3))
print('plotting')
fig = plt.figure(figsize=(40, 40), dpi=120)
spi = 0
for i, model_i in enumerate(models):
    for j, model_j in enumerate(models):
        spi += 1
        if model_j != model_i:

            plt.subplot(len(models), len(models), spi, xmargin=10, ymargin=10)
            labels_i = sorted(list(cooc_stats[model_i].keys()), reverse=True)
            labels_j = sorted(list(cooc_stats[model_j].keys()), reverse=True)
            if model_i == 'cards':
                labels_i.remove('0_0')
            x = np.zeros((len(labels_i), len(labels_j)))
            for li, label_i in enumerate(labels_i):
                for lj, label_j in enumerate(labels_j):
                    x[li][lj] = cooc_stats[model_i][label_i][model_j][label_j]

            plt.imshow(x, interpolation='none')
            plt.ylabel(model_i, rotation=90)
            plt.xlabel(model_j)
            plt.xticks(np.arange(len(labels_j)), labels_j, rotation=90, fontsize=6)
            plt.yticks(np.arange(len(labels_i)), labels_i, fontsize=6)


print('layout+show')
#fig.tight_layout()
#plt.show()

#
# {'cardiff-stance-climate': {'none': 401220, 'favor': 1066763},
#  'cardiff-offensive': {'offensive': 44852, 'not-offensive': 1423131},
#  'cardiff-sentiment': {'negative': 338337, 'neutral': 931686, 'positive': 197960},
#  'bertweet-sentiment': {'negative': 376438, 'neutral': 896848, 'positive': 194697},
#  'geomotions-orig': {'surprise': 1123856, 'confusion': 25457, 'curiosity': 33793, 'fear': 6928, 'disgust': 1904, 'gratitude': 19005, 'excitement': 6193, 'approval': 64845, 'anger': 6447, 'admiration': 43558, 'disappointment': 8950, 'remorse': 2729, 'neutral': 26889, 'amusement': 4420, 'desire': 6284, 'caring': 11081, 'annoyance': 20757, 'disapproval': 21612, 'sadness': 7641, 'embarrassment': 610, 'love': 4617, 'nervousness': 716, 'joy': 2920, 'pride': 14195, 'relief': 1316, 'optimism': 1073, 'realization': 186, 'grief': 1},
#  'geomotions-ekman': {'neutral': 1174550, 'fear': 8547, 'joy': 185387, 'disgust': 1488, 'surprise': 56472, 'anger': 34105, 'sadness': 7434},
#  'cardiff-emotion': {'anger': 386030, 'sadness': 306153, 'optimism': 579827, 'joy': 195973},
#  'bertweet-emotions': {'anger': 24782, 'others': 1236294, 'disgust': 109933, 'fear': 39790, 'joy': 49429, 'sadness': 2202, 'surprise': 5553},
#  'nrc': {'fear': 327889, 'anger': 238695, 'negative': 562795, 'sadness': 259782, 'disgust': 185412, 'anticipation': 329305, 'positive': 593580, 'trust': 403964, 'joy': 222798, 'surprise': 161042}
#  }
