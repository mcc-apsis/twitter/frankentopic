import os
import json
from collections import defaultdict
import logging

import requests.exceptions
from searchtweets import ResultStream

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.DEBUG)

# Q1 = '("the" OR "be" OR "to" OR "of" OR "and" OR "a" OR "in" OR "that" OR "have" OR "I" OR "it" OR ' \
#      '"for" OR "not" OR "on" OR "with" OR "he" OR "as" OR "you" OR "do" OR "at" OR "this" OR "but" OR ' \
#      '"his" OR "by" OR "from" OR "they" OR "we" OR "say" OR "her" OR "she" OR "or" OR "an" OR "will" OR ' \
#      '"my" OR "one" OR "all" OR "would" OR "there" OR "their" OR "what" OR "so" OR "up" OR "out" OR "if" OR ' \
#      '"about" OR "who" OR "get" OR "which" OR "go" OR "me" OR "when" OR "make" OR "can" OR "like" OR "time" OR ' \
#      '"no" OR "just" OR "him" OR "know" OR "take" OR "people" OR "into" OR "year" OR "your" OR "good" OR ' \
#      '"some" OR "could" OR "them" OR "see" OR "other" OR "than" OR "then" OR "now" OR "look" OR "only" OR ' \
#      '"come" OR "its" OR "over" OR "think" OR "also" OR "back" OR "after" OR "use" OR "two" OR "how" OR ' \
#      '"our" OR "work" OR "first" OR "well" OR "way" OR "even" OR "new" OR "want" OR "because" OR "any" OR ' \
#      '"these" OR "give" OR "day" OR "most" OR "us" OR http) ' \
#      'lang:en -is:retweet -is:quote'
# Q2 = '("the" OR "be" OR "to" OR "of" OR "and" OR "a" OR "in" OR "that" OR "have" OR "I" OR "it" OR ' \
#      '"for" OR "not" OR "on" OR "with" OR "he" OR "as" OR "you" OR "do" OR "at" OR "this" OR "but" OR ' \
#      '"his" OR "by" OR "from" OR "they" OR "we" OR "say" OR "her" OR "she" OR "or" OR "an" OR "will" OR ' \
#      '"my" OR "one" OR "all" OR "would" OR "there" OR "their" OR "what" OR "so" OR "up" OR "out" OR "if" OR ' \
#      '"about" OR "who" OR "get" OR "which" OR "go" OR "me" OR "when" OR "make" OR "can" OR "like" OR "time" OR ' \
#      '"no" OR "just" OR "him" OR "know" OR "take" OR "people" OR "into" OR "year" OR "your" OR "good" OR ' \
#      '"some" OR "could" OR "them" OR "see" OR "other" OR "than" OR "then" OR "now" OR "look" OR "only" OR ' \
#      '"come" OR "its" OR "over" OR "think" OR "also" OR "back" OR "after" OR "use" OR "two" OR "how" OR ' \
#      '"our" OR "work" OR "first" OR "well" OR "way" OR "even" OR "new" OR "want" OR "because" OR "any" OR ' \
#      '"these" OR "give" OR "day" OR "most" OR "us") ' \
#      'lang:en -is:retweet -is:quote'
# 
# targets = [
#     # ('q1_neg_d', f'-wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q1}', 'day'),
#     # ('q1_pos_d', f'wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q1}', 'day'),
#     ('q1_neg_h', f'-wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q1}', 'hour'),
#     ('q1_pos_h', f'wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q1}', 'hour'),
#     # ('q1_neg_m', f'-wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q1}', 'minute'),
#     # ('q1_pos_m', f'wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q1}', 'minute'),
# 
#     # ('q2_neg_d', f'-wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q2}', 'day'),
#     # ('q2_pos_d', f'wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q2}', 'day'),
#     ('q2_neg_h', f'-wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q2}', 'hour'),
#     ('q2_pos_h', f'wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q2}', 'hour'),
#     # ('q2_neg_m', f'-wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q2}', 'minute'),
#     # ('q2_pos_m', f'wrbvjxxuqgekwmyvoufhogxkycophhuigc {Q2}', 'minute')
# ]

targets = [
    ('nen-nq',
     '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
     '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
     'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
     'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
     'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
     'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
     'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
     'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
     'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
     'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
     'these OR give OR day OR most OR us) ' \
     '-lang:en',
     'hour'),
    ('en-q',
     '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
     '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
     'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
     'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
     'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
     'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
     'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
     'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
     'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
     'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
     'these OR give OR day OR most OR us) ' \
     'lang:en is:quote',
     'hour'),
    ('en-nq',
     '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
     '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
     'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
     'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
     'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
     'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
     'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
     'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
     'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
     'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
     'these OR give OR day OR most OR us) ' \
     'lang:en -is:quote',
     'hour'),
    ('nen-q',
     '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
     '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
     'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
     'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
     'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
     'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
     'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
     'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
     'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
     'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
     'these OR give OR day OR most OR us) ' \
     '-lang:en is:quote',
     'hour'),

    # ('en_nrt_nq',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  'lang:en -is:retweet -is:quote',
    #  'hour'),
    # ('en',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  'lang:en',
    #  'hour'),
    # ('rt_re_q',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  '(is:retweet OR is:reply OR is:quote)',
    #  'hour'),
    # ('en_rt_re_q',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  'lang:en (is:retweet OR is:reply OR is:quote)',
    #  'hour'),
    # ('nen_rt_re_q',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  '-lang:en (is:retweet OR is:reply OR is:quote)',
    #  'hour'),
    # ('rt',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  'is:retweet',
    #  'hour'),
    # ('q',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  'is:quote',
    #  'hour'),
    # ('re',
    #  '-wrbvjxxuqgekwmyvoufhogxkycophhuigc '
    #  '(the OR be OR to OR of OR "and" OR a OR in OR that OR have OR I OR it OR ' \
    #  'for OR not OR on OR with OR he OR as OR you OR do OR at OR this OR but OR ' \
    #  'his OR by OR from OR they OR we OR say OR her OR she OR "or" OR an OR will OR ' \
    #  'my OR one OR all OR would OR there OR their OR what OR so OR up OR out OR if OR ' \
    #  'about OR who OR get OR which OR go OR me OR when OR make OR can OR like OR time OR ' \
    #  'no OR just OR him OR know OR take OR people OR into OR year OR your OR good OR ' \
    #  'some OR could OR them OR see OR other OR than OR then OR now OR look OR only OR ' \
    #  'come OR its OR over OR think OR also OR back OR after OR use OR two OR how OR ' \
    #  'our OR work OR first OR well OR way OR even OR new OR want OR because OR any OR ' \
    #  'these OR give OR day OR most OR us) ' \
    #  'is:reply',
    #  'hour'),
]

raw = {}
cnts = {}

for key, QUERY, gran in targets:
    logging.info(f'> Searching for: {key} ==============================================================')
    try:
        stream = ResultStream(
            endpoint='https://api.twitter.com/2/tweets/counts/all',
            request_parameters={
                'query': QUERY,
                'start_time': '2022-06-01T00:00:00Z',
                'end_time': '2022-06-03T00:00:00Z',
                'granularity': gran
            },
            bearer_token=os.getenv("TWITTER"),
            output_format='r')
        raw[key] = []
        for results in stream.stream():
            logging.info('Received page.')
            if 'data' in results and type(results['data']) == list:
                raw[key].append(results)
            else:
                logging.error('Something went wrong!')
    except requests.exceptions.HTTPError:
        logging.error('meh.')

    cnts[key] = defaultdict(int)
    for di in raw[key]:
        for dii in di['data']:
            cnts[key][dii['start'][:10]] += dii['tweet_count']

print(' | '.join([tk for tk, _, _ in targets]))
for k in cnts[targets[0][0]].keys():
    prts = [f'{cnts[tk][k]:,}' for tk, _, _ in targets]
    print(f'{k}: {" | ".join(prts)}')
