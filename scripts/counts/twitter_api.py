import os
import json
import logging
from searchtweets import ResultStream

logging.basicConfig(format='%(asctime)s [%(levelname)s] %(name)s: %(message)s', level=logging.DEBUG)
# NAME = 'cc_Jan2017_Sept2022'
# QUERY = '"climate change" lang:en -is:retweet -is:quote'
NAME = 'en_Jan2018_Dec2021_neg'
QUERY = '-wrbvjxxuqgekwmyvoufhogxkycophhuigc ("the" OR "be" OR "to" OR "of" OR "and" OR "a" OR "in" OR "that" OR "have" OR "I" OR "it" OR ' \
        '"for" OR "not" OR "on" OR "with" OR "he" OR "as" OR "you" OR "do" OR "at" OR "this" OR "but" OR ' \
        '"his" OR "by" OR "from" OR "they" OR "we" OR "say" OR "her" OR "she" OR "or" OR "an" OR "will" OR ' \
        '"my" OR "one" OR "all" OR "would" OR "there" OR "their" OR "what" OR "so" OR "up" OR "out" OR "if" OR ' \
        '"about" OR "who" OR "get" OR "which" OR "go" OR "me" OR "when" OR "make" OR "can" OR "like" OR "time" OR ' \
        '"no" OR "just" OR "him" OR "know" OR "take" OR "people" OR "into" OR "year" OR "your" OR "good" OR ' \
        '"some" OR "could" OR "them" OR "see" OR "other" OR "than" OR "then" OR "now" OR "look" OR "only" OR ' \
        '"come" OR "its" OR "over" OR "think" OR "also" OR "back" OR "after" OR "use" OR "two" OR "how" OR ' \
        '"our" OR "work" OR "first" OR "well" OR "way" OR "even" OR "new" OR "want" OR "because" OR "any" OR ' \
        '"these" OR "give" OR "day" OR "most" OR "us") ' \
        'lang:en -is:retweet -is:quote'


stream = ResultStream(
    endpoint='https://api.twitter.com/2/tweets/counts/all',
    request_parameters={
        'query': QUERY,
        # 'start_time': '2017-01-01T00:00:00Z',
        # 'end_time': '2022-10-10T23:59:59Z',
        'start_time': '2018-01-01T00:00:00Z',
        'end_time': '2018-01-03T00:00:00Z',
        # 'end_time': '2021-12-31T23:59:59Z',
        'granularity': 'minute'
    },
    bearer_token=os.getenv("TWITTER"),
    output_format='r')

logging.info(f'Searching for: {QUERY}')
FILE = f'data/counts/{NAME}.jsonl'
logging.info(f'Writing to: {FILE}')
if os.path.exists(FILE):
    raise FileExistsError('File already exists. If you are certain you want to proceed, rename or delete the file.')

with open(FILE, 'w') as f_out:
    for results in stream.stream():
        logging.info('Received page.')
        if 'data' in results and type(results['data']) == list:
            f_out.write(json.dumps(results) + '\n')
            f_out.flush()
        else:
            logging.error('Something went wrong!')
